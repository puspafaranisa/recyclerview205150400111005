package com.example.tugas5_205150400111005;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {

    TextView nama_tv, NIM_tv;
    ImageView foto_iv;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        nama_tv = findViewById(R.id.nama);
        NIM_tv = findViewById(R.id.NIM);
        foto_iv = findViewById(R.id.foto);

        String nama = getIntent().getStringExtra("keyNama");
        String NIM = getIntent().getStringExtra("keyNIM");
        int foto = getIntent().getIntExtra("keyFoto", 0);

        nama_tv.setText(nama);
        NIM_tv.setText("NIM : " + NIM);
        if(foto == 2131165271) //perempuan
        {
            foto_iv.setImageResource(R.drawable.man);
        }
        else //laki-laki
        {
            foto_iv.setImageResource(R.drawable.woman);
        }

    }
}